﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichas
{
    class MainFichas
    {
        static void Main(string[] args)
        {
            Fichas f = new Fichas();

            f.SolicitarNumeroFichas();

            f.CrearFichas();

            Console.ReadKey();
        }
    }
}