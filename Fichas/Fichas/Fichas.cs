﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichas
{
    class Fichas
    {
        /*Ficha: |1:1|
          Ficha: |2:1||2:2|
          Ficha: |3:1||3:2||3:3|*/

        private int rangoFichas, numeroFichas;

        public void SolicitarNumeroFichas()
        {
            Console.Write("Ingrese el folio que desea imprimir: ");
            numeroFichas = int.Parse(Console.ReadLine());
        }

        public void CrearFichas()
        {
            rangoFichas = 1;

            for (int i = rangoFichas; i <= numeroFichas; i++)
            {
                Console.Write("Ficha: ");

                for (int j = 1; j <= rangoFichas; j++)
                {
                    Console.Write($"|{i}:{j}|");
                }

                rangoFichas++;

                Console.WriteLine();
            }
        }
    }
}